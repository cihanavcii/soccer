-- Schemas
-- ---------------------------------------------------------

create schema if not exists public;



-- Extensions
-- ---------------------------------------------------------

create extension if not exists "uuid-ossp";



-- Datamodel
-- ---------------------------------------------------------

-- Users Table
drop table if exists public.t_users cascade;
create table public.t_users (
	pk uuid primary key default uuid_generate_v4() not null,
	email text unique,
	password text not null,
	name text unique not null,
	is_admin boolean not null
);

-- Users view
drop view if exists public.v_users cascade;
create view public.v_users as
select
	pk,
	email,
	password,
	name,
	is_admin as "isAdmin"
from public.t_users;

-- Country table
drop table if exists public.t_countrys cascade;
create table public.t_countrys (
	pk uuid primary key default uuid_generate_v4() not null,
	name text unique not null,
	image_url text not null
);
insert into public.t_countrys (name, image_url)
values ('Turkey', 'http://localhost:1903/public/img/turk.jpg
'),
('Amerika', 'http://localhost:1903/public/img/amerika.png
' ),
('Almanya', 'http://localhost:1903/public/img/almanya.png
');

-- Country view
drop view if exists public.v_countrys cascade;
create view public.v_countrys as
select
	pk,
	name,
    image_url as "imageUrl"
from public.t_countrys;


-- Teams table
drop table if exists public.t_teams cascade;
create table public.t_teams (
	pk uuid primary key default uuid_generate_v4() not null,
	name text not null,
	country_pk uuid references public.t_countrys(pk) on update cascade on delete cascade not null,
	date_of_foundation date,
	color1 text,
	color2 text,
	city text not null,
	email text not null,
	phone numeric not null,
	image_url text,
	match numeric,
	win numeric,
	lose numeric,
	goals_scored numeric,
	conceded_goal numeric,
	point numeric,
	likes numeric
);

-- Teams view
drop view if exists public.v_teams cascade;
create view public.v_teams as
select
	pk,
	name,
	country_pk as "countryPk",
	date_of_foundation as "dateOfFoundation",
	color1,
	color2,
	city,
	email,
	phone,
	image_url as "imageUrl",
	match,
	win,
	lose,
	goals_scored as "goalsScored",
	conceded_goal as "concededGoal",
	point,
	likes
from public.t_teams;


-- Positions table
drop table if exists public.t_positions cascade;
create table public.t_positions (
	pk uuid primary key default uuid_generate_v4() not null,
	position text not null
);
insert into public.t_positions (position)
values ('KL'),
('DF'),
('OS'),
('SGK'),
('SLK'),
('FV');

-- Positions view
drop view if exists public.v_positions cascade;
create view public.v_positions as
select
	pk,
	position
from public.t_positions;


-- Players Table
drop table if exists public.t_players cascade;
create table public.t_players (
	pk uuid primary key default uuid_generate_v4() not null,
	position_pk uuid references public.t_positions(pk) on update cascade on delete cascade not null,
	team_pk uuid references public.t_teams(pk) on update cascade on delete cascade,
    user_pk uuid references public.t_users(pk) on update cascade on delete cascade,
    country_pk uuid references public.t_countrys(pk) on update cascade on delete cascade not null,
	name text not null,
    surname text not null,
    age numeric not null,
    height numeric,
    weight numeric,
    phone numeric,
    email text,
	image_url text
	
);

-- Players view
drop view if exists public.v_players cascade;
create view public.v_players as
select
	pk,
	position_pk as "positionPk",
	team_pk as "teamPk",
	user_pk as "userPk",
	country_pk as "countryPk",
	name,
	surname,
	age,
	height,
	weight,
	phone,
	email,
	image_url as "imageUrl"
	
from public.t_players;


--  Areas table
drop table if exists public.t_areas cascade;
create table public.t_areas (
	pk uuid primary key default uuid_generate_v4() not null,
	name text not null,
	adress text not null,
	city text not null,
	phone numeric not null,
	email text,
	capacity numeric,
	image_url text,
	image_class text,
	unique (name, city, phone)
);


-- Areas view
drop view if exists public.v_areas cascade;
create view public.v_areas as
select
	pk,
	name,
	adress,
	city,
	phone,
	email,
	capacity,
    image_url as "imageUrl",
	image_class as "imageClass"
from public.t_areas;


-- Match table
drop table if exists public.t_matchs cascade;
create table public.t_matchs (
	pk uuid primary key default uuid_generate_v4() not null,
	area_pk uuid references public.t_areas(pk) on update cascade on delete cascade not null,
	first_team_pk uuid references public.t_teams(pk) on update cascade on delete cascade not null,
	second_team_pk uuid references public.t_teams(pk) on update cascade on delete cascade,
	user_pk uuid references public.t_users(pk) on update cascade on delete cascade,
	date timestamp not null,
	current_player numeric,
	missing_player numeric,
    unique (first_team_pk, second_team_pk, date)
);


-- Match view
drop view if exists public.v_matchs cascade;
create view public.v_matchs as
select
	pk,
	area_pk as "areaPk",
	first_team_pk as "firstTeamPk",
	second_team_pk as "secondTeamPk",
	user_pk as "userPk",
	date,
	current_player as "currentPlayer",
	missing_player as "missingPlayer"
from public.t_matchs;


-- Forum table
drop table if exists public.t_forums cascade;
create table public.t_forums (
	pk uuid primary key default uuid_generate_v4() not null,
	image_url text,
	video_url text,
	interview text
);


-- Forum view
drop view if exists public.v_forums cascade;
create view public.v_forums as
select
	pk,
	image_url as "imageUrl",
	video_url as "videoUrl",
	interview
from public.t_forums;


-- Match2media table
drop table if exists public.t_match2medias cascade;
create table public.t_match2medias (
	pk uuid primary key default uuid_generate_v4() not null,
	match_pk uuid references public.t_matchs(pk) on update cascade on delete cascade,
	forum_pk uuid references public.t_forums(pk) on update cascade on delete cascade
);


-- Match2media view
drop view if exists public.v_match2medias cascade;
create view public.v_match2medias as
select
	pk,
	match_pk as "matchPk",
	forum_pk as "forumPk"
from public.t_match2medias;

