import { config } from './config';
import { logger } from './logger';
import { createConnection } from 'typeorm';

const connectionOptions: any = config.db.connection;


export async function connectDb() {
    
    let retries = 5;
    let dbConnection;
    return new Promise ( async (resolve) => {
        
        while (retries) {
            try {
                dbConnection = await createConnection(connectionOptions);
                logger.info(`Database: Successfully connected to ${config.db.connection.type}.`)
                return resolve(dbConnection)
            } catch (error) {
                retries -= 1;
                logger.error(error);
                logger.error(`Database: retries left: ${retries}`);
                await new Promise(res => setTimeout(res, 10000));
            }
        }
        
        return resolve(false)
    })
};
