import { Entity, PrimaryColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Match } from './match';
import { Forum } from './forum';


@Entity({ name: 'public.v_match2medias' })
export class Match2media {
 
    @PrimaryColumn()
    pk?: string;


    @ManyToOne(type => Match, match => match.match2mediaPk)
    @JoinColumn({
        name: 'matchPk'
    })
    matchPk?: string;


    @ManyToOne(type => Forum, forum => forum.match2mediaPk)
    @JoinColumn({
        name: 'forumPk'
    })
    forumPk?: string;
}
