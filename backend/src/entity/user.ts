import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Player } from './player';
import { Match } from './match';

@Entity({ name: 'public.v_users' })
export class User {

    @PrimaryColumn()
    pk?: string;

    @Column()
    email?: string;

    @Column()
    password?: string;

    @Column()
    name?: string;

    @Column({ nullable: false })
    isAdmin?: boolean;

    @OneToMany(type => Player, player => player.userPk)
    playerPk: Player[];

    @OneToMany(type => Match, match => match.userPk)
    matchPk: Match[];
}
