import { Entity, Column, PrimaryColumn, JoinColumn, ManyToOne } from 'typeorm';
import { Position } from './position';
import { User } from './user';
import { Country } from './country';
import { Team } from './team';



@Entity({ name: 'public.v_players' })
export class Player {

    @PrimaryColumn()
    pk?: string;

    @ManyToOne(type => Position, position => position.playerPk)
    @JoinColumn({
        name: 'positionPk'
    })
    positionPk?: string;

    @ManyToOne(type => User, user => user.playerPk)
    @JoinColumn({
        name: 'userPk'
    })
    userPk?: string;

    @ManyToOne(type => Country, country => country.playerPk)
    @JoinColumn({
        name: 'countryPk'
    })
    countryPk?: string;

    @ManyToOne(type => Team, team => team.playerPk)
    @JoinColumn({
        name: 'teamPk'
    })
    teamPk?: string;

    @Column()
    name?: string;

    @Column()
    surname?: string;

    @Column()
    age?: string;

    @Column()
    height?: string;

    @Column()
    weight?: string;

    @Column()
    phone?: string;

    @Column()
    email?: string;

    @Column()
    imageUrl?: string;

}
