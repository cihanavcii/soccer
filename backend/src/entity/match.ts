import { Entity, Column, PrimaryColumn, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { Area } from './area';
import { Match2media } from './match2media';
import { Team } from './team';
import { User } from './user';


@Entity({ name: 'public.v_matchs' })
export class Match {

    @PrimaryColumn()
    pk?: string;

    @ManyToOne(type => Area, area => area.matchPk)
    @JoinColumn({
        name: 'areaPk'
    })
    areaPk?: string;

    @ManyToOne(type => User, user => user.matchPk)
    @JoinColumn({
        name: 'userPk'
    })
    userPk?: string;

    @ManyToOne(type => Team, team => team.matchPk)
    @JoinColumn({
        name: 'firstTeamPk'
    })
    firstTeamPk?: string;

    @ManyToOne(type => Team, team => team.secondTeamPk)
    @JoinColumn({
        name: 'secondTeamPk'
    })
    secondTeamPk?: string;

    @Column()
    date?: string;

    @Column()
    currentPlayer?: string;

    @Column()
    missingPlayer?: string;

    @OneToMany(type => Match2media, match2media => match2media.matchPk)
    match2mediaPk: Match2media[];
}