import { Entity, Column, PrimaryColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import { Match } from './match';
import { Player } from './player';
import { Country } from './country';



@Entity({ name: 'public.v_teams' })
export class Team {

    @PrimaryColumn()
    pk?: string;

    @Column()
    name?: string;

    @ManyToOne(type => Country, country => country.teamPk)
    @JoinColumn({
        name: 'countryPk'
    })
    countryPk?: string;

    @Column()
    dateOfFoundation?: string;

    @Column()
    color1?: string;

    @Column()
    color2?: string;

    @Column()
    city?: string;

    @Column()
    email?: string;

    @Column()
    phone?: string;

    @Column()
    imageUrl?: string;

    @Column()
    match?: string;

    @Column()
    win?: string;

    @Column()
    lose?: string;

    @Column()
    goalsScored?: string;

    @Column()
    concededGoal?: string;

    @Column()
    point?: string;

    @Column()
    likes?: string;

    @OneToMany(type => Player, player => player.teamPk)
    playerPk: Player[];

    @OneToMany(type => Match, match => match.firstTeamPk)
    matchPk: Match[];

    @OneToMany(type => Match, match => match.secondTeamPk)
    secondTeamPk: Match[];
}
