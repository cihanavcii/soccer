import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Match2media } from './match2media';


@Entity({ name: 'public.v_forums' })
export class Forum {
 
    @PrimaryColumn()
    pk?: string;

    @Column()
    imageUrl?: string;

    @Column()
    videoUrl?: string;

    @Column()
    interview?: string;

    @OneToMany(type => Match2media, match2media => match2media.forumPk)
    match2mediaPk: Match2media[];
}
