import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Player } from './player';



@Entity({ name: 'public.v_positions' })
export class Position {

    @PrimaryColumn()
    pk?: string;

    @Column()
    position?: string;

    @OneToMany(type => Player, player => player.positionPk)
    playerPk: Player[];
}
