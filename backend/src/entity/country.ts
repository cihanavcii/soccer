import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Player } from './player';
import { Team } from './team';



@Entity({ name: 'public.v_countrys' })
export class Country {
 
    @PrimaryColumn()
    pk?: string;

    @Column()
    name?: string;

    @Column()
    imageUrl?: string;

    @OneToMany(type => Player, player => player.countryPk)
    playerPk: Player[];

    @OneToMany(type => Team, team => team.countryPk)
    teamPk: Team[];
}
