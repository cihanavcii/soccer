import { Entity, Column, PrimaryColumn, OneToMany } from 'typeorm';
import { Match } from './match';



@Entity({ name: 'public.v_areas' })
export class Area {

    @PrimaryColumn()
    pk?: string;

    @Column()
    name?: string;

    @Column()
    adress?: string;

    @Column()
    city?: string;

    @Column()
    phone?: string;

    @Column()
    email?: string;

    @Column()
    capacity?: string;

    @Column()
    imageUrl?: string;

    @Column()
    imageClass?: string;

    @OneToMany(type => Match, match => match.areaPk)
    matchPk: Match[];
}
