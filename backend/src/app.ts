
require('dotenv').config({ path: 'http://localhost:1903/../.env' });

import 'reflect-metadata';
import express = require('express');
import morgan = require('morgan');
import bodyParser = require('body-parser');
import compression = require('compression');
import fileUpload = require('express-fileupload');
import { config } from './config'
import { logger } from './logger';
import { connectDb } from './db';
import { positionRouter, playerRouter, areaRouter, matchRouter, mediaRouter, userRouter, countryRouter, teamRouter, match2mediaRouter, forumRouter } from './route';
import { errorResponses } from './assets';

const port = process.env.API_PORT
const app: express.Application = express();
export var dbConnection: any;


// Start app
const startApp = async () => {

    // connact to database
    dbConnection = await connectDb();
    if (dbConnection) {

        logger.info(`successfully connected to ${config.db.connection.type}`)

        // Cors configurations 
        app.use((req: any, res: any, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept, Authorization'
            );
            if (req.method === 'OPTIONS') {
                res.header('Access-Control-Allow-Methods',
                    'PUT, POST, PATCH, DELETE, GET');
                return res.status(200).json({});
            }
            next();
        });

        // Environment
        app.get('env');

        // Develop log
        if (process.env.API_ENV !== 'production') {
            app.use(morgan('dev'));
        }

        // Parse response bodies
        app.use(bodyParser.urlencoded({
            extended: true
        }));
        app.use(bodyParser.json());

        // Compression
        app.use(compression());

        // Enable express file upload
        app.use(fileUpload({
            createParentPath: true,
            limits: { fileSize: 500 * 1024 * 1024 }
        }));

        // Public files
        app.use('/public', express.static(__dirname + '/../public'));

        // Routes
        app.use(`/${config.api.version}`, positionRouter);
        app.use(`/${config.api.version}`, playerRouter);
        app.use(`/${config.api.version}`, areaRouter);
        app.use(`/${config.api.version}`, matchRouter);
        app.use(`/${config.api.version}`, mediaRouter);
        app.use(`/${config.api.version}`, userRouter);
        app.use(`/${config.api.version}`, countryRouter);
        app.use(`/${config.api.version}`, teamRouter);
        app.use(`/${config.api.version}`, forumRouter);
        app.use(`/${config.api.version}`, match2mediaRouter);

        // Errors
        app.use((error: any, req: any, res: any, next: any) => {

            if (error.status === 400) {
                res.status(error.status);
                res.json(error);
            }
            else if (error.status === 401) {
                res.status(errorResponses.accessDenied.status);
                res.json(errorResponses.accessDenied.error);
            } else if (error.status === 403) {
                res.status(errorResponses.unauthorized.status);
                res.json(errorResponses.unauthorized.error);
            }
        })

        app.use((req, res) => {
            res.status(errorResponses.notFound.status);
            res.json(errorResponses.notFound.error);
        });
        app.listen(port, () => {

            if (process.env.API_ENV === 'production') {
                logger.info(`Server: Successfully running express server on port: ${port} in production mode.`);
            } else {
                logger.info(`Server: Successfully running express server on port: ${port} in develop mode.`);
            }
        })
    }
}
startApp();

export default app;
