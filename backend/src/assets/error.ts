export var errorResponses = {
    missingField: {
        status: 400,
        error: {
            statusCode: 400,
            dbErrorCode: '23502',
            message: 'Bad request!',
            description: 'Missing field!',
            field: ''
        }
    },
    missingFile: {
        status: 400,
        error: {
            statusCode: 400,
            message: 'Bad request!',
            description: 'Missing file!',
            fileType: ''
        }
    },
    missingRelation: {
        status: 400,
        error: {
            statusCode: 400,
            dbErrorCode: '23503',
            message: 'Bad request!',
            description: 'Relation does not exist!',
            field: ''
        }
    },
    accessDenied: {
        status: 401,
        error: {
            statusCode: 401,
            message: 'Access denied!',
        }
    },
    unauthorized: {
        status: 403,
        error: {
            statusCode: 403,
            message: 'Unauthorized!',
        }
    },
    notFound: {
        status: 404,
        error: { 
            statusCode: 404,
            message: 'Not found!'
        }
    },
    resourceNotFound: {
        status: 404,
        error: { 
            statusCode: 404,
            message: 'Requested resource can not be found!'
        }
    },
    duplication: {
        status: 409,
        error: {
            statusCode: 409,
            dbErrorCode: '23505',
            message: 'Duplication error!',
            details: ''
        }
    },
    userAlreadyExists: {
        status: 409,
        error: {
            statusCode: 409,
            message: 'User already exists!'
        }
    },
    userCreationFailed: {
        status: 500,
        error: {
            statusCode: 500,
            message: 'Failed to create user!'
        }
    }
} 