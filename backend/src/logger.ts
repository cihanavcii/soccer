
import * as winston from 'winston';


export const logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: 'logs/warning.log', level: 'warning' }),
        new winston.transports.File({ filename: 'logs/combined.log', level: 'info'}),
    ],
});

if (process.env.API_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple(),
    }));
}
