
require('dotenv').config();


export const config = {
    api: {
        version: 'v1'
    },
    db: {
        connection: {
            type: 'postgres',
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_SOCCER_DB,
            entities: [
                __dirname + '/entity/*.ts',
                __dirname + '/entity/*.js'
            ],
            synchronize: false,
            logging: false
        }
    },
    mediaLocations: {
        images: './public/img/',
        videos: './public/vid/',
        imageUrl: 'http://localhost:1903/public/img/',
        videoUrl: 'http://localhost:1903/public/vid/'
    }
}
