import express = require('express');
import { Request, Response } from 'express';
import { getPositions, getPosition, postPosition, patchPosition, deletePosition } from '../controller';
import { dbConnection } from '../app';

export var positionRouter = express.Router();

// GET REQUEST
positionRouter.get('/positions', async function (req: Request, res: Response) {
    let controllerRes: any = await getPositions(dbConnection)
    res.status(controllerRes.status).json(controllerRes.data);
});

// GET BY PK
positionRouter.get('/positions/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getPosition(dbConnection, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});

// POST REQUEST
positionRouter.post('/positions', async function (req: Request, res: Response) {
    let controllerRes: any = await postPosition(dbConnection, req);
    res.status(controllerRes.status).json(controllerRes.data);
});

// PATCH REQUEST
positionRouter.patch('/positions/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchPosition(dbConnection, req, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});

// DELETE REQUEST
positionRouter.delete('/positions/:pk' ,async function (req: Request, res: Response) {
    let controllerRes: any = await deletePosition(dbConnection, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});
