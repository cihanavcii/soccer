import express = require('express');
import { Request, Response } from 'express';
import { getUsers, getUser, postUser, patchUser, deleteUser } from '../controller';
import { dbConnection } from '../app';

export var userRouter = express.Router();


userRouter.get('/users', async function (req: Request, res: Response) {
    let controllerRes: any = await getUsers(dbConnection)
    res.status(controllerRes.status).json(controllerRes.data);
});

userRouter.get('/users/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getUser(dbConnection, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});

userRouter.post('/users', async function (req: Request, res: Response) {
    let controllerRes: any = await postUser(dbConnection, req);
    res.status(controllerRes.status).json(controllerRes.data);
});

userRouter.patch('/users/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchUser(dbConnection, req, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});

userRouter.delete('/users/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteUser(dbConnection, req.params.pk);
    res.status(controllerRes.status).json(controllerRes.data);
});
