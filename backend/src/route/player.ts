import express = require('express');
import { Request, Response } from 'express';
import { getPlayers, getPlayer, postPlayer, patchPlayer, deletePlayer } from '../controller';
import { dbConnection } from '../app';

export var playerRouter = express.Router();

// GET REQUEST
playerRouter.get('/players', async function (req: Request, res: Response) {
    let controllerRes: any = await getPlayers(dbConnection)
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    }, 2000);
});

// GET BY PK
playerRouter.get('/players/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getPlayer(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
playerRouter.post('/players', async function (req: Request, res: Response) {
    let controllerRes: any = await postPlayer(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST
playerRouter.patch('/players/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchPlayer(dbConnection, req, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
playerRouter.delete('/players/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deletePlayer(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});
