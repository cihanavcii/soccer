import express = require('express');
import { Request, Response } from 'express';
import { getCountrys, postCountry, patchCountry, deleteCountry, getCountryByName } from '../controller';
import { dbConnection } from '../app';

export var countryRouter = express.Router();

// GET REQUEST
countryRouter.get('/countrys', async function (req: Request, res: Response) {
    let controllerRes: any = await getCountrys(dbConnection)
    res.status(controllerRes.status).json(controllerRes.data);
});

// GET BY NAME
countryRouter.get('/countrys/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await getCountryByName(dbConnection, req.params.name);
    res.status(controllerRes.status).json(controllerRes.data);
});

// POST REQUEST
countryRouter.post('/countrys', async function (req: Request, res: Response) {
    let controllerRes: any = await postCountry(dbConnection, req);
    res.status(controllerRes.status).json(controllerRes.data);
});

// PATCH REQUEST
countryRouter.patch('/countrys/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await patchCountry(dbConnection, req, req.params.name);
    res.status(controllerRes.status).json(controllerRes.data);
});

// DELETE REQUEST
countryRouter.delete('/countrys/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteCountry(dbConnection, req.params.name);
    res.status(controllerRes.status).json(controllerRes.data);
});
