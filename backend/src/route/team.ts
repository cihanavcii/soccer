import express = require('express');
import { Request, Response } from 'express';
import { getTeams, getCountryByName, postTeam, patchTeam, deleteTeam, getTeam } from '../controller';
import { dbConnection } from '../app';

export var teamRouter = express.Router();

// GET REQUEST
teamRouter.get('/teams', async function (req: Request, res: Response) {
    let controllerRes: any = await getTeams(dbConnection)
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    }, 2000);
});

// GET BY PK
teamRouter.get('/teams/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getTeam(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// GET BY NAME
teamRouter.get('/teams/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await getCountryByName(dbConnection, req.params.name);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
teamRouter.post('/teams', async function (req: Request, res: Response) {
    let controllerRes: any = await postTeam(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST 
teamRouter.patch('/teams/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchTeam(dbConnection, req, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
teamRouter.delete('/teams/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteTeam(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});
