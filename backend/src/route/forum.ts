import express = require('express');
import { Request, Response } from 'express';
import { getForums, getForumByPk, postForum, patchForum, deleteForum } from '../controller';
import { dbConnection } from '../app';

export var forumRouter = express.Router();

// GET REQUEST
forumRouter.get('/forums', async function (req: Request, res: Response) {
    let controllerRes: any = await getForums(dbConnection)
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    }, 2000);
});

// GET BY PK
forumRouter.get('/forums/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getForumByPk(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
forumRouter.post('/forums', async function (req: Request, res: Response) {
    let controllerRes: any = await postForum(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST
forumRouter.patch('/forums/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchForum(dbConnection, req, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
forumRouter.delete('/forums/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteForum(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});
