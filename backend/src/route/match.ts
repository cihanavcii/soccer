import express = require('express');
import { Request, Response } from 'express';
import { getMatchs, getMatch, postMatch, patchMatch, deleteMatch } from '../controller';
import { dbConnection } from '../app';

export var matchRouter = express.Router();

// GET REQUEST
matchRouter.get('/matchs', async function (req: Request, res: Response) {
    let controllerRes: any = await getMatchs(dbConnection)
    //setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    //}, 2000);
});

// GET BY PK
matchRouter.get('/matchs/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getMatch(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
matchRouter.post('/matchs', async function (req: Request, res: Response) {
    let controllerRes: any = await postMatch(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST
matchRouter.patch('/matchs/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchMatch(dbConnection, req, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
matchRouter.delete('/matchs/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteMatch(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});
