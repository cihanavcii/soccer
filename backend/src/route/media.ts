import express = require('express');
import { Request, Response } from 'express';
import { postImage, postVideo } from '../controller';

export var mediaRouter = express.Router();


mediaRouter.post('/media/img', async function (req: Request, res: Response) {
    let controllerRes: any = await postImage(req);
    res.status(controllerRes.status).json(controllerRes.data);
});

mediaRouter.post('/media/vid', async function (req: Request, res: Response) {
    let controllerRes: any = await postVideo(req);
    res.status(controllerRes.status).json(controllerRes.data);
});
 