import express = require('express');
import { Request, Response } from 'express';
import { getMatch2medias, getMatch2mediaByPk, postMatch2media, patchMatch2media, deleteMatch2media } from '../controller';
import { dbConnection } from '../app';

export var match2mediaRouter = express.Router();

// GET REQUEST
match2mediaRouter.get('/match2medias', async function (req: Request, res: Response) {
    let controllerRes: any = await getMatch2medias(dbConnection)
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    }, 2000);
});

// GET BY PK
match2mediaRouter.get('/match2medias/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await getMatch2mediaByPk(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
match2mediaRouter.post('/match2medias', async function (req: Request, res: Response) {
    let controllerRes: any = await postMatch2media(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST
match2mediaRouter.patch('/match2medias/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await patchMatch2media(dbConnection, req, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
match2mediaRouter.delete('/match2medias/:pk', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteMatch2media(dbConnection, req.params.pk);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});
