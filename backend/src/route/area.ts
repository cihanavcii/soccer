import express = require('express');
import { Request, Response } from 'express';
import { getAreas, postArea, patchArea, deleteArea, getAreaByName } from '../controller';
import { dbConnection } from '../app';

export var areaRouter = express.Router();

// GET REQUEST
areaRouter.get('/areas', async function (req: Request, res: Response) {
    let controllerRes: any = await getAreas(dbConnection)
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes);
    }, 2000);
});

// GET BY NAME
areaRouter.get('/areas/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await getAreaByName(dbConnection, req.params.name);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// POST REQUEST
areaRouter.post('/areas', async function (req: Request, res: Response) {
    let controllerRes: any = await postArea(dbConnection, req);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// PATCH REQUEST
areaRouter.patch('/areas/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await patchArea(dbConnection, req, req.params.name);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);
});

// DELETE REQUEST
areaRouter.delete('/areas/:name', async function (req: Request, res: Response) {
    let controllerRes: any = await deleteArea(dbConnection, req.params.name);
    setTimeout(function () {
        res.status(controllerRes.status).json(controllerRes.data);
    }, 2000);

});
