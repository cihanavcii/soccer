import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Position } from '../entity';
import { errorResponses } from '../assets';



/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getPositions(dbConnection: any) {

    let positionRepository = dbConnection.getRepository(Position);

    return new Promise(async (resolve) => {

        try {

            let getPositions = await positionRepository.find({
                relations: ['playerPk']
            });
            return resolve({
                status: 200,
                data: getPositions
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}


/** GET BY PK
 * @param {any} dbConnection
 */
export async function getPosition(dbConnection: any, pk: string) {

    let positionRepository = dbConnection.getRepository(Position);

    return new Promise(async (resolve) => {

        try {

            let getPositions = await positionRepository.find({
                where: {
                    pk: pk
                }
            });

            if ( getPositions.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getPositions
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postPosition(dbConnection: any, req: Request) {

    let body = req.body;
    let positionRepository = dbConnection.getRepository(Position);
    let position = new Position();

    position.pk = uuid();
    position.position = body.position;

    return new Promise(async (resolve) => {

        // Save data
        try {
            let postPosition = await positionRepository.save(position);
            return resolve({
                status: 201,
                data: postPosition
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchPosition(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let positionRepository = dbConnection.getRepository(Position);
        let position: Position[];

        try {
            position = await positionRepository.find({
                where: {
                    pk: pk
                }
            });

            if (position.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            position[0].pk = pk;
            position[0].position = body.position;

            let postPosition = await positionRepository.save(position);
            return resolve({
                status: 200,
                data: postPosition
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deletePosition(dbConnection: any, pk: string) {

    let positionRepository = dbConnection.getRepository(Position);
    let position: Position[];

    return new Promise(async (resolve) => {

        try {
            position = await positionRepository.find({ where: { pk: pk } });

            if (position.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deletePosition = await positionRepository.remove(position);
            return resolve({
                status: 204,
                data: deletePosition
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}