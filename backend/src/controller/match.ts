import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Match } from '../entity';
import { errorResponses } from '../assets';;


/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getMatchs(dbConnection: any) {

    let matchRepository = dbConnection.getRepository(Match);

    return new Promise(async (resolve) => {

        try {
            let text = "";

            let getMatchs = await matchRepository.find({
                relations: ['areaPk', 'userPk', 'firstTeamPk', 'secondTeamPk', 'match2mediaPk'],
                order: {
                    date: "ASC",
                }
            });

            // CONSTRUCT A DATE TO MATCH ARRAY

            // 1. Get uniqe dates
            const uniqueDates = [...new Set(getMatchs.map((item: any) => new Date(item.date).toLocaleDateString()))];

            // Date to match array. Contains every uniqe date with all its matches
            let dateMatchArray = [];
            // 2. Loop through uniqe date array
            for (let i = 0; i < uniqueDates.length; i++) {

                // Matches of a uniqe date
                let matches = [];
                // 3. For every unique date, loop through database getMatchs array
                for (let x = 0; x < getMatchs.length; x++) {

                    // If a match has the same date as the uniqe date, push it into the match array of the uniqe date
                    if (uniqueDates[i] === new Date(getMatchs[x].date).toLocaleDateString()) {
                        matches.push(getMatchs[x]);
                    }
                }

                // Push the uniqe date with all its matches to the date to match array
                dateMatchArray.push({
                    date: uniqueDates[i],
                    matches: matches
                })
            }

            return resolve({
                status: 200,
                //data: getMatchs
                data: dateMatchArray
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}


/** GET BY PK
 * @param {any} dbConnection 
 */
export async function getMatch(dbConnection: any, pk: string) {

    let matchRepository = dbConnection.getRepository(Match);

    return new Promise(async (resolve) => {

        try {

            let getMatchs = await matchRepository.find({
                relations: ['areaPk', 'userPk', 'firstTeamPk', 'secondTeamPk'],
                where: {
                    pk: pk
                }
            });

            if (getMatchs.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getMatchs
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST
 * @param {any} dbConnection 
 * @param {any} body 
 */
export async function postMatch(dbConnection: any, req: Request) {

    let body = req.body;
    let matchRepository = dbConnection.getRepository(Match);
    let match = new Match();

    match.pk = uuid();
    match.areaPk = body.areaPk;
    match.firstTeamPk = body.firstTeamPk;
    match.secondTeamPk = body.secondTeamPk;
    match.date = body.date;
    match.currentPlayer = body.currentPlayer;
    match.missingPlayer = body.missingPlayer;

    return new Promise(async (resolve) => {
        // Save data
        try {
            let postMatch = await matchRepository.save(match);
            return resolve({
                status: 201,
                data: postMatch
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else if (error.code === errorResponses.missingRelation.error.dbErrorCode) {
                errorResponses.missingRelation.error.field = error.detail;
                return resolve({
                    status: errorResponses.missingRelation.status,
                    data: { error: errorResponses.missingRelation.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection 
 * @param {any} body
 * @param {string} pk 
 */
export async function patchMatch(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let matchRepository = dbConnection.getRepository(Match);
        let match: Match[];

        // Update data
        try {
            match = await matchRepository.find({
                where: {
                    pk: pk
                }
            });

            if (match.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            match[0].pk = pk;
            match[0].areaPk = body.areaPk;
            match[0].firstTeamPk = body.firstTeamPk;
            match[0].secondTeamPk = body.secondTeamPk;
            match[0].date = body.date;
            match[0].currentPlayer = body.currentPlayer;
            match[0].missingPlayer = body.missingPlayer;

            let postMatch = await matchRepository.save(match);
            return resolve({
                status: 200,
                data: postMatch
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteMatch(dbConnection: any, pk: string) {

    let matchRepository = dbConnection.getRepository(Match);
    let match: Match[];

    return new Promise(async (resolve) => {

        try {
            match = await matchRepository.find({ where: { pk: pk } });

            if (match.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteMatch = await matchRepository.remove(match);
            return resolve({
                status: 204,
                data: deleteMatch
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}