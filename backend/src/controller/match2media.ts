import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Match2media } from '../entity';
import { errorResponses } from '../assets';


/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getMatch2medias(dbConnection: any) {

    let match2mediaRepository = dbConnection.getRepository(Match2media);

    return new Promise(async (resolve) => {

        try {

            let getMatch2medias = await match2mediaRepository.find({
                relations: ['matchPk', 'forumPk']
            });
            return resolve({
                status: 200,
                data: getMatch2medias
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY PK
 * @param {any} dbConnection
 */
export async function getMatch2mediaByPk(dbConnection: any, pk: string) {

    let match2mediaRepository = dbConnection.getRepository(Match2media);

    return new Promise(async (resolve) => {

        try {

            let getMatch2medias = await match2mediaRepository.find({
                where: {
                    pk: pk
                }
            });

            if (getMatch2medias.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getMatch2medias
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST 
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postMatch2media(dbConnection: any, req: Request) {

    let body = req.body;
    let match2mediaRepository = dbConnection.getRepository(Match2media);
    let match2media = new Match2media();

    match2media.pk = uuid();
    match2media.matchPk = body.matchPk;
    match2media.forumPk = body.forumPk;

    return new Promise(async (resolve) => {
        // Save data
        try {
            let postMatch2 = await match2mediaRepository.save(match2media);
            return resolve({
                status: 201,
                data: postMatch2
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchMatch2media(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let match2mediaRepository = dbConnection.getRepository(Match2media);
        let match2media: Match2media[];

        try {
            match2media = await match2mediaRepository.find({
                where: {
                    pk: pk
                }
            });

            if (match2media.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            match2media[0].pk = pk;
            match2media[0].matchPk = body.matchPk;
            match2media[0].forumPk = body.forumPk;

            let postMatch2 = await match2mediaRepository.save(match2media);
            return resolve({
                status: 200,
                data: postMatch2
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteMatch2media(dbConnection: any, name: string) {

    let match2mediaRepository = dbConnection.getRepository(Match2media);
    let match2media: Match2media[];

    return new Promise(async (resolve) => {

        try {
            match2media = await match2mediaRepository.find({ where: { name: name } });

            if (match2media.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteMatch2 = await match2mediaRepository.remove(match2media);
            return resolve({
                status: 204,
                data: deleteMatch2
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}
