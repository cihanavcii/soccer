import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Area } from '../entity';
import { errorResponses } from '../assets';
import { postImage } from './index';
import { config } from '../config';



/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getAreas(dbConnection: any) {

    let areaRepository = dbConnection.getRepository(Area);

    return new Promise(async (resolve) => {
 
        try {

            let getAreas = await areaRepository.find({
                relations: ['matchPk']
            });
            return resolve({
                status: 200,
                data: getAreas
            })


        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY NAME
 * @param {any} dbConnection
 * @param {string} name proimary key of area 
 */
export async function getAreaByName(dbConnection: any, name: string) {

    let areaRepository = dbConnection.getRepository(Area);

    return new Promise(async (resolve) => {

        try {

            let getAreas = await areaRepository.find({
                where: {
                    name: name
                }
            });

            if (getAreas.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getAreas
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY PK
 * @param {any} dbConnection
 * @param {string} pk proimary key of area 
 */
export async function getAreaByPk(dbConnection: any, pk: string) {

    let areaRepository = dbConnection.getRepository(Area);

    return new Promise(async (resolve) => {

        try {

            let getAreas = await areaRepository.find({
                where: {
                    pk: pk
                }
            });

            if (getAreas.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getAreas
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postArea(dbConnection: any, req: Request) {

    let body = req.body;
    let areaRepository = dbConnection.getRepository(Area);
    let area = new Area();

    area.pk = uuid();
    area.name = body.name;
    area.adress = body.adress;
    area.city = body.city;
    area.phone = body.phone;
    area.email = body.email;
    area.capacity = body.capacity;
    area.imageUrl = body.imageUrl;
    area.imageClass = body.imageClass;
    console.log(area)
    return new Promise(async (resolve) => {

        // Save image
        if (req.files) {
            try {
                let imageRes: any = await postImage(req);
                if (imageRes.data.fileName) {
                    area.imageUrl = `${config.mediaLocations.imageUrl}${imageRes.data.fileName}`;
                }

            } catch (error) {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }

        // Save data
        try {
            let postArea = await areaRepository.save(area);
            return resolve({
                status: 201,
                data: postArea
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchArea(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let areaRepository = dbConnection.getRepository(Area);
        let area: Area[];

        try {
            area = await areaRepository.find({
                where: {
                    pk: pk
                }
            });

            if (area.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            area[0].pk = pk;
            area[0].name = body.name;
            area[0].adress = body.adress;
            area[0].city = body.city;
            area[0].phone = body.phone;
            area[0].email = body.email;
            area[0].capacity = body.capacity;
            area[0].imageUrl = body.imageUrl;
            area[0].imageClass = body.imageClass;

            let postArea = await areaRepository.save(area);
            return resolve({
                status: 200,
                data: postArea
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteArea(dbConnection: any, name: string) {

    let areaRepository = dbConnection.getRepository(Area);
    let area: Area[];

    return new Promise(async (resolve) => {

        try {
            area = await areaRepository.find({ where: { name: name } });

            if (area.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteArea = await areaRepository.remove(area);
            return resolve({
                status: 204,
                data: deleteArea
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}
