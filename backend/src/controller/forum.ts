import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Forum } from '../entity';
import { errorResponses } from '../assets';
import { postImage } from './index';
import { config } from '../config';
import { postVideo } from './media';



/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getForums(dbConnection: any) {

    let forumRepository = dbConnection.getRepository(Forum);

    return new Promise(async (resolve) => {

        try {

            let getForums = await forumRepository.find({
                relations: ['match2mediaPk']
            });
            return resolve({
                status: 200,
                data: getForums
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY PK
 * @param {any} dbConnection
 */
export async function getForumByPk(dbConnection: any, pk: string) {

    let forumRepository = dbConnection.getRepository(Forum);

    return new Promise(async (resolve) => {

        try {

            let getForums = await forumRepository.find({
                where: {
                    pk: pk
                }
            });

            if (getForums.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getForums
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST 
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postForum(dbConnection: any, req: Request) {

    let body = req.body;
    let forumRepository = dbConnection.getRepository(Forum);
    let forum = new Forum();

    forum.pk = uuid();
    forum.imageUrl = body.imageUrl;
    forum.videoUrl = body.videoUrl;
    forum.interview = body.interview;

    return new Promise(async (resolve) => {

        // Save image
        if (req.files) {
            try {

                let imageRes: any = await postImage(req);
                let videoRes: any = await postVideo(req);

                if (imageRes.data.fileName) {
                    forum.imageUrl = `${config.mediaLocations.imageUrl}${imageRes.data.fileName}`;
                }

                if (videoRes.data.fileName) {
                    forum.videoUrl = `${config.mediaLocations.videoUrl}${videoRes.data.fileName}`;
                }

            } catch (error) {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }

        // Save data
        try {
            let postForum = await forumRepository.save(forum);
            return resolve({
                status: 201,
                data: postForum
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchForum(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let forumRepository = dbConnection.getRepository(Forum);
        let forum: Forum[];

        try {
            forum = await forumRepository.find({
                where: {
                    pk: pk
                }
            });

            if (forum.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            forum[0].pk = pk;
            forum[0].imageUrl = body.imageUrl;
            forum[0].videoUrl = body.videoUrl;
            forum[0].interview = body.interview;

            let postForum = await forumRepository.save(forum);
            return resolve({
                status: 200,
                data: postForum
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteForum(dbConnection: any, name: string) {

    let forumRepository = dbConnection.getRepository(Forum);
    let forum: Forum[];

    return new Promise(async (resolve) => {

        try {
            forum = await forumRepository.find({ where: { name: name } });

            if (forum.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteForum = await forumRepository.remove(forum);
            return resolve({
                status: 204,
                data: deleteForum
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}
