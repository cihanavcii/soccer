import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Country } from '../entity';
import { errorResponses } from '../assets';
import { postImage } from './index';
import { config } from '../config';



/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getCountrys(dbConnection: any) {

    let countryRepository = dbConnection.getRepository(Country);

    return new Promise(async (resolve) => {

        try {

            let getCountrys = await countryRepository.find({
                relations: ['playerPk', 'teamPk']
            });
            return resolve({
                status: 200,
                data: getCountrys
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY NAME
 * @param {any} dbConnection
 * @param {string} name proimary key of country 
 */
export async function getCountryByName(dbConnection: any, name: string) {

    let countryRepository = dbConnection.getRepository(Country);

    return new Promise(async (resolve) => {

        try {

            let getCountrys = await countryRepository.find({
                where: {
                    name: name
                }
            });

            if (getCountrys.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getCountrys
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY PK
 * @param {any} dbConnection
 */
export async function getCountryByPk(dbConnection: any, pk: string) {

    let countryRepository = dbConnection.getRepository(Country);

    return new Promise(async (resolve) => {

        try {

            let getCountrys = await countryRepository.find({
                where: {
                    pk: pk
                }
            });

            if (getCountrys.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getCountrys
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST 
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postCountry(dbConnection: any, req: Request) {

    let body = req.body;
    let countryRepository = dbConnection.getRepository(Country);
    let country = new Country();

    country.pk = uuid(); 
    country.name = body.name;
    country.imageUrl = body.imageUrl;

    return new Promise(async (resolve) => {

        // Save image
        if (req.files) {
            try {

                let imageRes: any = await postImage(req);

                if (imageRes.data.fileName) {
                    country.imageUrl = `${config.mediaLocations.imageUrl}${imageRes.data.fileName}`;
                }

            } catch (error) {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }

        // Save data
        try {
            let postCountry = await countryRepository.save(country);
            return resolve({
                status: 201,
                data: postCountry
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchCountry(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let countryRepository = dbConnection.getRepository(Country);
        let country: Country[];

        try {
            country = await countryRepository.find({
                where: {
                    pk: pk
                }
            });

            if (country.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            country[0].pk = pk;
            country[0].name = body.name;
            country[0].imageUrl = body.imageUrl;

            let postCountry = await countryRepository.save(country);
            return resolve({
                status: 200,
                data: postCountry
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteCountry(dbConnection: any, name: string) {

    let countryRepository = dbConnection.getRepository(Country);
    let country: Country[];

    return new Promise(async (resolve) => {

        try {
            country = await countryRepository.find({ where: { name: name } });

            if (country.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteCountry = await countryRepository.remove(country);
            return resolve({
                status: 204,
                data: deleteCountry
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}
