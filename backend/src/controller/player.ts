import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Player } from '../entity';
import { errorResponses } from '../assets';
import { postImage } from './index';
import { config } from '../config';


/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getPlayers(dbConnection: any) {

    let playerRepository = dbConnection.getRepository(Player);

    return new Promise(async (resolve) => {

        try {

            let getPlayers = await playerRepository.find({
                relations: ['positionPk', 'userPk', 'countryPk', 'teamPk']
            });

            return resolve({
                status: 200,
                data: getPlayers

            })
        } catch (error) {
            return resolve({
                status: 500,
                error: error
            })
        }
    })
}


/** GET BY PK
 * @param {any} dbConnection 
 */
export async function getPlayer(dbConnection: any, pk: string) {

    let playerRepository = dbConnection.getRepository(Player);

    return new Promise(async (resolve) => {

        try {

            let getPlayers = await playerRepository.find({
                relations: ['positionPk', 'userPk', 'countryPk', 'teamPk'],

                where: {
                    pk: pk
                }
            });

            if (getPlayers.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getPlayers
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST
 * @param {any} dbConnection 
 * @param {any} body 
 */
export async function postPlayer(dbConnection: any, req: Request) {
    let body = req.body;
    let playerRepository = dbConnection.getRepository(Player);
    let player = new Player();

    player.pk = uuid();
    player.positionPk = body.positionPk;
    player.countryPk = body.countryPk;
    player.userPk = body.userPk;
    player.teamPk = body.teamPk;
    player.name = body.name;
    player.surname = body.surname;
    player.age = body.age;
    player.height = body.height;
    player.weight = body.weight;
    player.phone = body.phone;
    player.email = body.email;
    player.imageUrl = body.imageUrl;

    return new Promise(async (resolve) => {

        // Save image
        if (req.files) {
            try {

                let imageRes: any = await postImage(req);

                if (imageRes.data.fileName) {
                    player.imageUrl = `${config.mediaLocations.imageUrl}${imageRes.data.fileName}`;
                }

            } catch (error) {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }

        // Save data
        try {
            let postPlayer = await playerRepository.save(player);
            return resolve({
                status: 201,
                data: postPlayer
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else if (error.code === errorResponses.missingRelation.error.dbErrorCode) {
                errorResponses.missingRelation.error.field = error.detail;
                return resolve({
                    status: errorResponses.missingRelation.status,
                    data: { error: errorResponses.missingRelation.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection 
 * @param {any} body
 * @param {string} pk 
 */
export async function patchPlayer(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let playerRepository = dbConnection.getRepository(Player);
        let player: Player[];

        // Update data
        try {
            player = await playerRepository.find({
                where: {
                    pk: pk
                }
            });

            if (player.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            player[0].pk = pk;
            player[0].positionPk = body.positionPk;
            player[0].countryPk = body.countryPk;
            player[0].userPk = body.userPk;
            player[0].teamPk = body.teamPk;
            player[0].name = body.name;
            player[0].surname = body.surname;
            player[0].age = body.age;
            player[0].height = body.height;
            player[0].weight = body.weight;
            player[0].phone = body.phone;
            player[0].email = body.email;
            player[0].imageUrl = body.imageUrl;

            let postPlayer = await playerRepository.save(player);
            return resolve({
                status: 200,
                data: postPlayer
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deletePlayer(dbConnection: any, pk: string) {

    let playerRepository = dbConnection.getRepository(Player);
    let player: Player[];

    return new Promise(async (resolve) => {

        try {
            player = await playerRepository.find({ where: { pk: pk } });

            if (player.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deletePlayer = await playerRepository.remove(player);
            return resolve({
                status: 204,
                data: deletePlayer
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}