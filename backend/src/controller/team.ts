import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { Team } from '../entity';
import { errorResponses } from '../assets';
import { postImage } from './index';
import { config } from '../config';



/** GET REQUEST
 * @param {any} dbConnection
 */
export async function getTeams(dbConnection: any) {

    let teamRepository = dbConnection.getRepository(Team);

    return new Promise(async (resolve) => {

        try {

            let getTeams = await teamRepository.find({
                relations: ['playerPk', 'matchPk', 'countryPk']
            });
            return resolve({
                status: 200,
                data: getTeams
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY NAME
 * @param {any} dbConnection
 * @param {string} name proimary key of team 
 */
export async function getTeamByName(dbConnection: any, name: string) {

    let teamRepository = dbConnection.getRepository(Team);

    return new Promise(async (resolve) => {

        try {

            let getTeams = await teamRepository.find({
                relations: ['playerPk', 'matchPk', 'countryPk'],
                where: {
                    name: name
                }
            });

            if (getTeams.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getTeams
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** GET BY PK
 * @param {any} dbConnection 
 */
export async function getTeam(dbConnection: any, pk: string) {

    let teamRepository = dbConnection.getRepository(Team);

    return new Promise(async (resolve) => {

        try {

            let getTeams = await teamRepository.find({
                relations: ['playerPk', 'matchPk', 'countryPk', ],

                where: {
                    pk: pk
                }
            });

            if (getTeams.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getTeams
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/** POST REQUEST
 * @param {any} dbConnection
 * @param {any} body
 */
export async function postTeam(dbConnection: any, req: Request) {

    let body = req.body;
    let teamRepository = dbConnection.getRepository(Team);
    let team = new Team();

    team.pk = uuid();
    team.name = body.name;
    team.countryPk = body.countryPk;
    team.dateOfFoundation = body.dateOfFoundation;
    team.color1 = body.color1;
    team.color2 = body.color2;
    team.city = body.city;
    team.email = body.email;
    team.phone = body.phone;
    team.imageUrl = body.imageUrl;
    team.match = body.match;
    team.win = body.win;
    team.lose = body.lose;
    team.goalsScored = body.goalsScored;
    team.concededGoal = body.concededGoal;
    team.point = body.point;
    team.likes = body.likes;
    
    return new Promise(async (resolve) => {

        // Save image
        if (req.files) {
            try {

                let imageRes: any = await postImage(req);

                if (imageRes.data.fileName) {
                    team.imageUrl = `${config.mediaLocations.imageUrl}${imageRes.data.fileName}`;
                }

            } catch (error) {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }

        // Save data
        try {
            let postTeam = await teamRepository.save(team);
            return resolve({
                status: 201,
                data: postTeam
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/** PATCH REQUEST
 * @param {any} dbConnection
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchTeam(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let teamRepository = dbConnection.getRepository(Team);
        let team: Team[];

        try {
            team = await teamRepository.find({
                where: {
                    pk: pk
                }
            });

            if (team.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
            team[0].pk = pk;
            team[0].name = body.name;
            team[0].countryPk = body.countryPk;
            team[0].dateOfFoundation = body.dateOfFoundation;
            team[0].color1 = body.color1;
            team[0].color2 = body.color2;
            team[0].city = body.city;
            team[0].email = body.email;
            team[0].phone = body.phone;
            team[0].imageUrl = body.imageUrl;
            team[0].match = body.match;
            team[0].win = body.win;
            team[0].lose = body.lose;
            team[0].goalsScored = body.goalsScored;
            team[0].concededGoal = body.concededGoal;
            team[0].point = body.point;
            team[0].likes = body.likes;

            let postTeam = await teamRepository.save(team);
            return resolve({
                status: 200,
                data: postTeam
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/** DELETE REQUEST
 * @param {any} dbConnection
 * @param {string} pk
 */
export async function deleteTeam(dbConnection: any, name: string) {

    let teamRepository = dbConnection.getRepository(Team);
    let team: Team[];

    return new Promise(async (resolve) => {

        try {
            team = await teamRepository.find({ where: { name: name } });

            if (team.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        try {
            let deleteTeam = await teamRepository.remove(team);
            return resolve({
                status: 204,
                data: deleteTeam
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}
