
import fs = require('fs');
import { Request } from 'express';
import { v4 as uuid } from 'uuid';
import { User } from '../entity';
import { errorResponses } from '../assets';



/**
 * @param {any} dbConnection 
 */
export async function getUsers(dbConnection: any) {

    let userRepository = dbConnection.getRepository(User);

    return new Promise(async (resolve) => {

        // Get users
        try {

            let getUsers = await userRepository.find({
                relations: ['playerPk', 'matchPk']
            });
            return resolve({
                status: 200,
                data: getUsers
            })

        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}


/**
 * @param {any} dbConnection
 */
export async function getUser(dbConnection: any, pk: string) {

    let userRepository = dbConnection.getRepository(User);

    return new Promise(async (resolve) => {

        // Get user by pk
        try {

            let getUsers = await userRepository.find({
                where: {
                    pk: pk
                }
            });

            if ( getUsers.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            return resolve({
                status: 200,
                data: getUsers
            })
        } catch (error) {

            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}

/**
 * @param {any} dbConnection 
 * @param {any} body 
 */
export async function postUser(dbConnection: any, req: Request) {

    let body = req.body;
    let userRepository = dbConnection.getRepository(User);
    let user = new User();

    user.pk = uuid();
    user.email = body.email;
    user.password = body.password;
    user.name = body.name;
    user.isAdmin = body.isAdmin;

    return new Promise(async (resolve) => {

        // Save data
        try {
            let postUser = await userRepository.save(user);
            return resolve({
                status: 201,
                data: postUser
            })
        } catch (error) {

            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else if (error.code === errorResponses.missingField.error.dbErrorCode) {
                errorResponses.missingField.error.field = error.column;
                return resolve({
                    status: errorResponses.missingField.status,
                    data: { error: errorResponses.missingField.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })

}

/**
 * @param {any} dbConnection 
 * @param {any} body 
 * @param {string} pk 
 */
export async function patchUser(dbConnection: any, req: Request, pk: string) {

    return new Promise(async (resolve) => {

        let body = req.body;
        let userRepository = dbConnection.getRepository(User);
        let user: User[];

        // Update data
        try {
            user = await userRepository.find({
                where: {
                    pk: pk
                }
            });

            if (user.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }

            user[0].pk = pk;
            user[0].email = body.email;
            user[0].password = body.password;
            user[0].name = body.name;
            user[0].isAdmin = body.isAdmin;

            let postUser = await userRepository.save(user);
            return resolve({
                status: 200,
                data: postUser
            })
        } catch (error) {

            // check unique constraint violation (duplicate of email)
            if (error.code === errorResponses.duplication.error.dbErrorCode) {
                errorResponses.duplication.error.details = error.detail;
                return resolve({
                    status: errorResponses.duplication.status,
                    data: { error: errorResponses.duplication.error }
                })
            } else {
                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        }
    })
}

/**
 * @param {any} dbConnection 
 * @param {string} pk 
 */
export async function deleteUser(dbConnection: any, pk: string) {

    let userRepository = dbConnection.getRepository(User);
    let user: User[];

    return new Promise(async (resolve) => {

        // Get user to remove
        try {
            user = await userRepository.find({ where: { pk: pk } });

            // Resource not found
            if (user.length < 1) {
                return resolve({
                    status: errorResponses.resourceNotFound.status,
                    data: { error: errorResponses.resourceNotFound.error }
                })
            }
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }

        // Remove user
        try {
            let deleteUser = await userRepository.remove(user);
            return resolve({
                status: 204,
                data: deleteUser
            })
        } catch (error) {
            return resolve({
                status: 500,
                data: { error: error }
            })
        }
    })
}