import fs = require('fs');
import { Request } from 'express';
import { config } from '../config';
import { errorResponses } from '../assets';


/**
 * @param {any} dbConnection
 * @param {any} body 
 */
export async function postImage(req: Request) {

    return new Promise(async (resolve) => {

        let image: any

        // Save image
        if (req.files) {

            try {
                image = req.files.image;
                image.mv(`${config.mediaLocations.images}${image.name}`);
                return resolve({
                    status: 201,
                    data: {
                        message: 'Successfully uploaded file!',
                        fileName: image.name
                    }
                })
            } catch (error) {

                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        } else {

            errorResponses.missingFile.error.fileType = 'image';
            return resolve({
                status: errorResponses.missingFile.status,
                data: { error: errorResponses.missingFile.error }
            })
        }

    })
}

/**
 * @param {any} dbConnection
 * @param {any} body 
 */
export async function deleteImage(req: Request) {

    return new Promise(async (resolve) => {

        let image: any

        // Delete image
        if (req.files) {

            try {
                image = req.files.image;
                fs.unlink(`${config.mediaLocations.images}${image.name}`, (err) => {
                    if (err) {
                        console.log("failed to delete local image:" + err);
                    } else {
                        console.log('successfully deleted local image');
                    }
                });
            } catch (error) {

                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        } else {

            errorResponses.missingFile.error.fileType = 'image';
            return resolve({
                status: errorResponses.missingFile.status,
                data: { error: errorResponses.missingFile.error }
            })
        }

    })
}
/**
 * @param {any} dbConnection
 * @param {any} body 
 */
export async function postVideo(req: Request) {

    return new Promise(async (resolve) => {

        let video: any

        // Save video
        if (req.files) {

            try {
                video = req.files.video;
                video.mv(`${config.mediaLocations.videos}${video.name}`);
                return resolve({
                    status: 201,
                    data: {
                        message: 'Successfully uploaded file!',
                        fileName: video.name
                    }
                })
            } catch (error) {

                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        } else {

            errorResponses.missingFile.error.fileType = 'video';
            return resolve({
                status: errorResponses.missingFile.status,
                data: { error: errorResponses.missingFile.error }
            })
        }

    })
}

/**
 * @param {any} dbConnection
 * @param {any} body 
 */
export async function deleteVideo(req: Request) {

    return new Promise(async (resolve) => {

        let video: any

        // Delete video
        if (req.files) {

            try {
                video = req.files.video;
                fs.unlink(`${config.mediaLocations.videos}${video.name}`, (err) => {
                    if (err) {
                        console.log("failed to delete local video:" + err);
                    } else {
                        console.log('successfully deleted local video');
                    }
                });
            } catch (error) {

                return resolve({
                    status: 500,
                    data: { error: error }
                })
            }
        } else {

            errorResponses.missingFile.error.fileType = 'video';
            return resolve({
                status: errorResponses.missingFile.status,
                data: { error: errorResponses.missingFile.error }
            })
        }

    })
}