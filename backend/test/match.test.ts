declare var require: any
const axios = require('axios').default;

import 'mocha';
import { expect, assert } from 'chai';
import config from './config.test';
const mlog = require('mocha-logger');

var requestOptions = {
    method: '',
    url: `${config.apiUrl}/${config.apiVersion}/matchs`,
    headers: {
        contentType: 'application/json',
        Authorization: 'Bearer sgsfgs'
    },
    data: {}
}

describe("matchs test", async () => {

    var postMatch: any;

    it('GET request test /v1/matchs', async function () {

        let response: any;
        requestOptions.method = 'get';

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 200, 'Status is not 200');
    });

    it('Should test to not null columns /v1/matchs', async function () {

        let response: any;
        requestOptions.method = 'post';
        requestOptions.headers.Authorization = `${config.bearerTokenValid}`;
        requestOptions.data = {
            maxPlayer: "12",
            minPlayer: "10"
        }

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 400, 'Status is not 400');
    });

    it('POST request test /v1/matchs', async function () {

        let response: any;
        requestOptions.method = 'post';
        requestOptions.headers.Authorization = `${config.bearerTokenValid}`;
        requestOptions.data = {
            date: "2020-03-31 9:30:25",
            maxPlayer: "12",
            minPlayer: "10"
        }

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        postMatch = response.data;

        assert.strictEqual(response.status, 201, 'Status is not 201');
    });

    it('Should test to unique columns /v1/matchs', async function () {

        let response: any;
        requestOptions.method = 'post';
        requestOptions.headers.Authorization = `${config.bearerTokenValid}`;
        requestOptions.data = {
            date: "2020-03-31 9:30:25",
        }

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 409, 'Status is not 409');
    });

    it('Should GET by pk with none existing pk /v1/matchs/:pk', async function () {

        let response: any;
        requestOptions.method = 'get';
        requestOptions.url = `${config.apiUrl}/${config.apiVersion}/matchs/0e916ec7-82f3-4c34-8688-ae6cea95801f`,
            requestOptions.headers.Authorization = `${config.bearerTokenValid}`;

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 404, 'Status is not 404');
    });

    it('Should GET by pk /v1/matchs/:pk', async function () {

        let response: any;
        requestOptions.method = 'get';
        requestOptions.url = `${config.apiUrl}/${config.apiVersion}/matchs/${postMatch.pk}`,
            requestOptions.headers.Authorization = `${config.bearerTokenValid}`;

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 200, 'Status is not 200');
    });

    it('PATCH request test /v1/matchs/:pk', async function () {

        let response: any;
        requestOptions.method = 'patch';
        requestOptions.url = `${config.apiUrl}/${config.apiVersion}/matchs/${postMatch.pk}`,
            requestOptions.headers.Authorization = `${config.bearerTokenValid}`;
        requestOptions.data = {
            date: "2020-09-17 1:00:25"
        }

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 200, 'Status is not 200');
    });

    it('DELETE request test /v1/matchs/:pk', async function () {

        let response: any;
        requestOptions.method = 'delete';
        requestOptions.url = `${config.apiUrl}/${config.apiVersion}/matchs/${postMatch.pk}`,
            requestOptions.headers.Authorization = `${config.bearerTokenValid}`;

        try {
            response = await axios(requestOptions);
        } catch (error) {
            response = error.response
        }
        assert.strictEqual(response.status, 204, 'Status is not 204');
    });
}); 