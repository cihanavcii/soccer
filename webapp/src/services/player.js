

export async function getAllPlayers() {
  let response;
  let data;
  let playerData;

  try {
    response = await fetch(`http://localhost:1903/v1/players`, {
      headers: {
        Authorization: "",
      },
    });

    data = await response.json();
    playerData = data

  } catch (error) {
    playerData = {
      error: error
    }
  }
  return new Promise((resolve) => { 
    return resolve(playerData)
  })
}

/**
* Post an player
* @param {formData} player player object 
*/
export async function postPlayer(player) {

  let requestOptions = {
    method: "POST",
    body: player,
  };
  let response;
  let data;
  let playerData;

  try {

    response = await fetch(
      "http://localhost:1903/v1/players",
      requestOptions
    );

    data = await response.json();

    if (response.status !== 201) {
      playerData = {
        error: 'Faild to post player!'
      }
    } else {
      playerData = {
        data: data
      }
    }
  } catch (error) {
    playerData = {
      error: error
    }
  }

  return new Promise((resolve) => {
    return resolve(playerData)
  })
}

/**
 * Patch an player
 * @param {formData} playerFormData player object 
 * @param {formData} playerPk player object 
 */
export async function patchPlayer(playerFormData, playerPk) {

  let requestOptions = {
    method: "PATCH",
    body: playerFormData

  };

  let response;
  let data;
  let playerData;

  try {
    response = await fetch(`http://localhost:1903/v1/players/${playerPk}`,
      requestOptions);

    data = await response.json();

    if (response.status !== 200) {
      playerData = {
        error: 'Faild to Patch Player'
      }
    } else {
      playerData = {
        data: data
      }
    }
  } catch (error) {
    playerData = {
      error: error
    }
  }
  return new Promise((resolve) => {
    return resolve(playerData)
  })
}

/**
 * Delete an player
 * @param {formData} playerPk player object 
 */
export async function deletePlayer(playerPk) {

  let response;
  let data;
  let playerData;

  try {
    response = await fetch(`http://localhost:1903/v1/players/${playerPk}`, {
      method: "DELETE",
    });
    data = await response.json();

    if (response.status !== 202 || data.length < 1) {
      playerData = {
        error: 'Delete Player Failed '
      }
    }
  } catch (error) {
    playerData = {
      error: error
    }
  }
  return new Promise((resolve) => {
    return resolve(playerData)
  })
}
