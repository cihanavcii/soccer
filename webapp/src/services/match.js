/**
 * Get all matchs
 */
export async function getAllMatchs() {
    let response;
    let data;
    let matchData;

    try {
        response = await fetch(`http://localhost:1903/v1/matchs`, {
            headers: {
                Authorization: "",
            },
        });

        data = await response.json();
        matchData = data

    } catch (error) {
        matchData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(matchData)
    })
}
/**
 * Post an match
 * @param {formData} match match object 
 */
export async function postMatch(match) {

    let requestOptions = {
        method: "POST",
        body: match,
    };
    let response;
    let data;
    let matchData;

    try {

        response = await fetch(
            "http://localhost:1903/v1/matchs",
            requestOptions
        );

        data = await response.json();

        if (response.status !== 201) {
            matchData = {
                error: 'Faild to post match!'
            }
            console.log(response)
        } else {
            matchData = {
                data: data
            } 
        }
    } catch (error) {
        matchData = {
            error: error
        }
    }

    return new Promise((resolve) => {
        return resolve(matchData)
    })
}

/**
 * Patch an match
 * @param {formData} matchFormData match object 
 * @param {formData} matchPk match object 
 */
export async function patchMatch(matchFormData, matchPk) {

    let requestOptions = {
        method: "PATCH",
        body: matchFormData

    };

    let response;
    let data;
    let matchData;

    try {
        response = await fetch(`http://localhost:1903/v1/matchs/${matchPk}`,
            requestOptions);

        data = await response.json();

        if (response.status !== 200) {
            matchData = {
                error: 'Faild to Patch match'
            }
        } else {
            matchData = {
                data: data
            }
        }
    } catch (error) {
        matchData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(matchData)
    })
}

/**
 * Delete an match
 * @param {formData} matchPk match object 
 */
export async function deleteMatch(matchPk) {

    let response;
    let data;
    let matchData;

    try {
        response = await fetch(`http://localhost:1903/v1/matchs/${matchPk}`, {
            method: "DELETE",
        });
        data = await response.json();

        if (response.status !== 202 || data.length < 1) {
            matchData = {
                error: 'Delete match Failed '
            }
        }
    } catch (error) {
        matchData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(matchData)
    })
}