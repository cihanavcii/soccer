/**
 * Get all match2medias
 */
export async function getAllMatch2medias() {
    let response;
    let data;
    let match2mediaData;

    try {
        response = await fetch(`http://localhost:1903/v1/match2medias`, {
            headers: {
                Authorization: "",
            },
        });

        data = await response.json();
        match2mediaData = data

    } catch (error) {
        match2mediaData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(match2mediaData)
    })
}
/**
 * Post an match2media
 * @param {formData} match2media match2media object 
 */
export async function postMatch2media(match2media) {

    let requestOptions = {
        method: "POST",
        body: match2media,
    };
    let response;
    let data;
    let match2mediaData;

    try {

        response = await fetch(
            "http://localhost:1903/v1/match2medias",
            requestOptions
        );

        data = await response.json();

        if (response.status !== 201) {
            match2mediaData = {
                error: 'Faild to post match2!'
            }
            console.log(response)
        } else {
            match2mediaData = {
                data: data
            }
        }
    } catch (error) {
        match2mediaData = {
            error: error
        }
    }

    return new Promise((resolve) => {
        return resolve(match2mediaData)
    })
}

/**
 * Patch an match2media
 * @param {formData} match2mediaFormData match2media object 
 * @param {formData} match2mediaPk match2media object 
 */
export async function patchMatch2media(match2mediaFormData, match2mediaPk) {

    let requestOptions = {
        method: "PATCH",
        body: match2mediaFormData

    };

    let response;
    let data;
    let match2mediaData;

    try {
        response = await fetch(`http://localhost:1903/v1/match2medias/${match2mediaPk}`,
            requestOptions);

        data = await response.json();

        if (response.status !== 200) {
            match2mediaData = {
                error: 'Faild to Patch match2'
            }
        } else {
            match2mediaData = {
                data: data
            }
        }
    } catch (error) {
        match2mediaData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(match2mediaData)
    })
}

/**
 * Delete an match2media
 * @param {formData} match2mediaPk match2media object 
 */
export async function deleteMatch2media(match2mediaPk) {

    let response;
    let data;
    let match2mediaData;

    try {
        response = await fetch(`http://localhost:1903/v1/match2medias/${match2mediaPk}`, {
            method: "DELETE",
        });
        data = await response.json();

        if (response.status !== 202 || data.length < 1) {
            match2mediaData = {
                error: 'Delete match2media Failed '
            }
        }
    } catch (error) {
        match2mediaData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(match2mediaData)
    })
}