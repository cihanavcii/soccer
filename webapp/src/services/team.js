/**
 * Get all teams
 */
export async function getAllTeams() {
    let response;
    let data;
    let teamData;

    try {
        response = await fetch(`http://localhost:1903/v1/teams`, {
            headers: {
                Authorization: "",
            },
        });

        data = await response.json();
        teamData = data

    } catch (error) {
        teamData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(teamData)
    })
}

/**
 * Post an team
 * @param {formData} team team object 
 */
export async function postTeam(team) {

    let requestOptions = {
        method: "POST",
        body: team,
    };
    let response;
    let data;
    let teamData;

    try {

        response = await fetch(
            "http://localhost:1903/v1/teams",
            requestOptions
        );

        data = await response.json();

        if (response.status !== 201) {
            teamData = {
                error: 'Faild to post team!'
            }
        }
        if (response.status == 409) {
            teamData = {
                error: 'Bu stad daha önce oluşturuldu!'
            }
        }
        else {
            teamData = {
                data: data
            }
        }
    } catch (error) {
        teamData = {
            error: error
        }
    }

    return new Promise((resolve) => {
        return resolve(teamData)
    })
}

/**
 * Patch an team
 * @param {formData} teamFormData team object 
 * @param {formData} teamPk team object 
 */
export async function patchTeam(teamFormData, teamPk) {

    let requestOptions = {
        method: "PATCH",
        body: teamFormData

    };

    let response;
    let data;
    let teamData;

    try {
        response = await fetch(`http://localhost:1903/v1/teams/${teamPk}`,
            requestOptions);

        data = await response.json(); 

        if (response.status !== 200) {
            teamData = {
                error: 'Faild to Patch team'
            }
        } else {
            teamData = {
                data: data
            }
        }
    } catch (error) {
        teamData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(teamData)
    })
}

/**
 * Delete an team
 * @param {formData} teamPk team object 
 */
export async function deleteTeam(teamPk) {

    let response;
    let data;
    let teamData;

    try {
        response = await fetch(`http://localhost:1903/v1/teams/${teamPk}`, {
            method: "DELETE",
        });
        data = await response.json();
        teamData = data

    } catch (error) {
        teamData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(teamData)
    })
}
