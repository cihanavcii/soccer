/**
 * Get all areas
 */
export async function getAllAreas() {
  let response;
  let data;
  let areaData;

  try {
    response = await fetch(`http://localhost:1903/v1/areas`, {
      headers: {
        Authorization: "",
      },
    });

    data = await response.json();
    areaData = data
    
  } catch (error) {
    areaData = {
      error: error
    }
  }
  return new Promise((resolve) => {
    return resolve(areaData)
  })
}

/**
 * Post an area
 * @param {formData} area area object 
 */
export async function postArea(area) {

  let requestOptions = {
    method: "POST",
    body: area,
  };
  let response;
  let data;
  let areaData;

  try {

    response = await fetch(
      "http://localhost:1903/v1/areas",
      requestOptions
    );

    data = await response.json();

    if (response.status !== 201) {
      areaData = {
        error: 'Faild to post area!'
      }
    }
    if (response.status == 409) {
      areaData = {
        error: 'Bu stad daha önce oluşturuldu!'
      }
    }
    else {
      areaData = {
        data: data
      }
    }
  } catch (error) {
    areaData = {
      error: error
    }
  }

  return new Promise((resolve) => {
    return resolve(areaData)
  })
}

/**
 * Patch an area
 * @param {formData} areaFormData area object 
 * @param {formData} areaPk area object 
 */
export async function patchArea(areaFormData, areaPk) {

  let requestOptions = {
    method: "PATCH",
    body: areaFormData

  };

  let response;
  let data;
  let areaData;

  try {
    response = await fetch(`http://localhost:1903/v1/areas/${areaPk}`,
      requestOptions);

    data = await response.json();

    if (response.status !== 200) {
      areaData = {
        error: 'Faild to Patch Area'
      }
    } else {
      areaData = {
        data: data
      }
    }
  } catch (error) {
    areaData = {
      error: error
    }
  }
  return new Promise((resolve) => {
    return resolve(areaData)
  })
}

/**
 * Delete an area
 * @param {formData} areaPk area object 
 */
export async function deleteArea(areaPk) {

  let response;
  let data;
  let areaData;

  try {
    response = await fetch(`http://localhost:1903/v1/areas/${areaPk}`, {
      method: "DELETE",
    });
    data = await response.json();
    areaData = data

  } catch (error) {
    areaData = {
      error: error
    }
  }
  return new Promise((resolve) => { 
    return resolve(areaData)
  })
}
