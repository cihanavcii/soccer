/**
 * Get all forums
 */
export async function getAllForums() {
    let response;
    let data;
    let forumData;

    try {
        response = await fetch(`http://localhost:1903/v1/forums`, {
            headers: {
                Authorization: "",
            },
        });

        data = await response.json();
        forumData = data

    } catch (error) {
        forumData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(forumData)
    })
}
/**
 * Post an forum
 * @param {formData} forum forum object 
 */
export async function postForum(forum) {

    let requestOptions = {
        method: "POST",
        body: forum,
    };
    let response;
    let data;
    let forumData;

    try {

        response = await fetch(
            "http://localhost:1903/v1/forums",
            requestOptions
        );

        data = await response.json();

        if (response.status !== 201) {
            forumData = {
                error: 'Faild to post forum!'
            }
            console.log(response)
        } else {
            forumData = {
                data: data
            }
        }
    } catch (error) {
        forumData = {
            error: error
        }
    }

    return new Promise((resolve) => {
        return resolve(forumData)
    })
}

/**
 * Patch an forum
 * @param {formData} forumFormData forum object 
 * @param {formData} forumPk forum object 
 */
export async function patchForum(forumFormData, forumPk) {

    let requestOptions = {
        method: "PATCH",
        body: forumFormData
    };

    let response;
    let data;
    let forumData;

    try {
        response = await fetch(`http://localhost:1903/v1/forums/${forumPk}`,
            requestOptions);

        data = await response.json();

        if (response.status !== 200) {
            forumData = {
                error: 'Faild to Patch forum'
            }
        } else {
            forumData = {
                data: data
            }
        }
    } catch (error) {
        forumData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(forumData)
    })
}

/**
 * Delete an forum
 * @param {formData} forumPk forum object 
 */
export async function deleteForum(forumPk) {

    let response;
    let data;
    let forumData;

    try {
        response = await fetch(`http://localhost:1903/v1/forums/${forumPk}`, {
            method: "DELETE",
        });
        data = await response.json();

        if (response.status !== 202 || data.length < 1) {
            forumData = {
                error: 'Delete forum Failed '
            }
        }
    } catch (error) {
        forumData = {
            error: error
        }
    }
    return new Promise((resolve) => {
        return resolve(forumData)
    })
}