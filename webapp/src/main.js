import "bootstrap";
import "../node_modules/bootswatch/dist/litera/bootstrap.min.css";
import "jquery";
import "popper.js";

import Vue from '../node_modules/vue'
import App from './App.vue'
import VueRouter from '../node_modules/vue-router'
import Home from './views/Home.vue'
import Player from './views/Player.vue'
import Match from './views/Match.vue'
import Area from './views/Area.vue'
import AreaDetail from './views/AreaDetail.vue'
import MatchDetail from './views/MatchDetail.vue'
import PlayerDetail from './views/PlayerDetail.vue'
import Team from './views/Team.vue'
import TeamDetail from './views/TeamDetail.vue'
import Forum from './views/Forum.vue'
import Images from './views/Images.vue'
import Video from './views/Video.vue'
import Interview from './views/Interview.vue'
import ImagesDetail from './views/ImagesDetail.vue'
import VideoDetail from './views/VideoDetail.vue'
import InterviewDetail from './views/InterviewDetail.vue'



Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [{
        path: '/',
        component: Home
    },
    {
        path: '/match',
        component: Match,
    },
    {
        path: '/area',
        component: Area,
    },
    {
        path: '/area/:areaPk',
        name: 'AreaDetail',
        component: AreaDetail,
        props: true
    },
    {
        path: '/match/:matchPk',
        name: 'MatchDetail',
        component: MatchDetail,
        props: true
    },
    {
        path: '/player/:playerPk',
        name: 'PlayerDetail',
        component: PlayerDetail,
        props: true
    },
    {
        path: '/team/:teamPk',
        name: 'TeamDetail',
        component: TeamDetail,
        props: true
    },
    {
        path: '/player',
        component: Player
    },
    {
        path: '/team',
        component: Team
    },
    {
        path: '/forum',
        component: Forum
    },
    {
        path: '/forum/images',
        component: Images
    },
    {
        path: '/forum/video',
        component: Video
    },
    {
        path: '/forum/interview',
        component: Interview
    },
    {
        path: '/forum/images/:matchPk',
        name: 'ImagesDetail',
        component: ImagesDetail,
        props: true
    },
    {
        path: '/forum/videos/:matchPk',
        name: 'VideoDetail',
        component: VideoDetail,
        props: true
    },
    {
        path: '/forum/interview/:matchPk',
        name: 'InterviewDetail',
        component: InterviewDetail,
        props: true
    },
    ],
    mode: 'history'
})


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')